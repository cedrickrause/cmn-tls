import {setTimeoutAsync} from "./cmn-tls";
it("Promise resolves after ms", async () => {
    const timeStart = process.hrtime()[0];
    await setTimeoutAsync(1000);
    const timeEnd = process.hrtime()[0];
    const timeElapsed: number =  timeEnd - timeStart;
    expect(timeElapsed).toEqual(1);
});