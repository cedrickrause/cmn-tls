/**
 * @module cmn-tls
 * Returns a promise which resolves after ms
 * @param ms The time (in milliseconds) that should elapse before the promise is resolved.
 * @example
 * ```typescript
 * 
 * console.log("Please wait a second");
 * setTimeoutAsync(1000).then(() => console.log("Done"))
 * ```
 */
export function setTimeoutAsync(ms: number): Promise<void> {
    return new Promise<void>((resolve) => {
        setTimeout(() => {
            resolve();
        }, ms);
    });
}