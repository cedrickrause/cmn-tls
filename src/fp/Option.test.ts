import { Option } from "./Option";
import { None, Some } from "../cmn-tls";

interface TestInterface {
    a?: string;
    b?: number;
    c?: number[];
    d?: string;
}
it("Option.create returns None if the passed value is null", () =>{
    const option = Option.create(null);
    expect(option).toBeInstanceOf(None);
});

it("Option.create returns Some if the passed value is not null", () => {
    const option = Option.create(10);
    expect(option).toBeInstanceOf(Some);
});

it("Some.value returns the actual value", () => {
    const value = "test";
    const option = new Some(value);
    expect(option.value).toEqual(value);
});

it("Some.map applies the mapping to the value", () => {
    const option = new Some(10);
    const mappedValue = option.map(v => v * 2).value;
    expect(option.value).toEqual(10);
});

it("Some.getOrElse returns the actual value", () => {
    const value = "test";
    const option = new Some(value);
    expect(option.getOrElse("fallback")).toEqual(value);
});

it("Some.getOrElse does not call defaultValueFactory", () => {
    const defaultValueFactory = jest.fn().mockReturnValue(10);
    const option = new Some<number>(1);
    option.getOrElse(defaultValueFactory);
    expect(defaultValueFactory).not.toBeCalled();
});

it("Some.hasValue returns true", () => {
    const x = Option.create(10);
    expect(new Some(1).hasValue()).toBeTruthy();
})

it("None.value returns null", () => {
    const option = new None();
    expect(option.value).toBeNull();
});

it("None.map returns a new None", () => {
    const option = new None<number>();
    expect(option.map(v => v * 2)).toBeInstanceOf(None);
});

it("None.getOrElse returns the default value", () =>{
    const option = new None<number>();
    expect(option.getOrElse(10)).toEqual(10);
});

it("None.getOrElse returns the value provided by the defaultValueFactory", () => {
    const option = new None();
    expect(option.getOrElse(() => 5)).toEqual(5);
});

it("None.hasValue returns false", () => {
    expect(new None().hasValue()).toBeFalsy();
});

it("optionize works correctly on primitive types", () =>{
    expect(Option.optionize(11)).toEqual(11);
});

it("optionize works correctly on arrays", () => {
    const array = [10, 20, 30];
    const expectedResult = [new Some(10), new Some(20), new Some(30)];
    expect(Option.optionize(array)).toEqual(expectedResult);
});

it("optionize works correctly on objects", () => {
    const obj: TestInterface = {
        a: "string",
        b: 10,
        c: [1,2,3],
    };
    const expectedResult = {
        a: new Some("string"),
        b: new Some(10),
        c: new Some([1,2,3]),
        d: new None()
    };
    expect(Option.optionize(obj, ["a", "b", "c", "d"])).toEqual(expectedResult);
});

it("optionize wotks correctly on functions", () =>{
    const fn = () => "";
    expect(Option.optionize(fn)).toEqual(fn);
});

it("optionize keeps methods working", () => {
    const testFn = jest.fn();
    const obj = {
        f: () => testFn()
    };
    const optionized = Option.optionize(obj, ["f"]);
    optionized.f();
    expect(testFn).toBeCalled();
});

it("Option.all returns Some if all values are instances of Some", () => {
    expect(Option.all([new Some(1), new Some(2)]).value).toEqual([1,2])
});

it("Option.all returns None if one value is an instance of None", () => {
    expect(Option.all([new Some(1), new None(), new Some(2)])).toEqual(new None());
});

it("Some.first returns first Some", () => {
    expect(Some.first(new None(), new Some(1), new None(), new Some(2))).toEqual(new Some(1));
});

it("Some.first returns None if the parameters doesn't contain a Some", () => {
    expect(Some.first(new None(), new None())).toEqual(new None());
});