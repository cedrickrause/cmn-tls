/**
 * @module cmn-tls
 * An instance of this class represents the result of the Try function.
 * * @typeparam T The type of the data which is wrapped by TryResult.
 */
export class TryResult<T>{

    /**
     * @ignore
     */
    private readonly data: T|Error;

    /**
     * @ignore
     */
    constructor(data: Error | T) {
        this.data = data;
    }

    /**
     * This function is used to recover a given error, similar to Promise.catch.
     * If the instance contains a result catch simply returns the result.
     * If the instance contains an error the handleError parameter is used to recover the error.
     * @param handleError The function is used to recover a possible error.
     */
    catch(handleError: (error: Error) => T): T{
        if(this.data instanceof Error){
            return handleError(this.data);
        }
        else if(this.data instanceof Promise){
            //Wrap the exception of the Promise
            return this.data.catch(error => handleError(error)) as any;
        }
        return this.data;
    }


}

/**index.htmlindeindex.htmlx.html
 * This functions tries to execute an expression and catches possible errors.
 * @typeparam T The type which is returned by expression.
 * @param expression The function that should be executed by the Try function.
 * @returns An instance of the TryResult<T> class. TryResult<T> contains the result of the given expression or the error which has been thrown by the expression.
 * @example
 * ```typescript
 * 
 * const num = Try(() => parseNumber("abc")).catch(ex => -1);// -1
 * const num2 = Try(() => parseNumber("11").catch(ex => -1));// 11
 * ```
 */
export function Try<T>(expression: () => T): TryResult<T> {
    try{
        const data = expression();
        return new TryResult<T>(data);
    }
    catch (e) {
        return new TryResult<T>(e);
    }
}