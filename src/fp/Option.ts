/**
 * @module cmn-tls
 */
import { Optionized } from "../types/Optionized";
import { isObject } from "util";

/**
 * An instance of this class represents an optional value.
 * It can be used intead of T|null.
 * @typeparam T The type of the contained value.
 */
export abstract class Option<T>{

    /**
     * Returns true if the instance contains a value.
     * This function works as typeguard.
     * @example
     * ```typescript
     * 
     * const option = Option.create(10)
     * //This will not work because number|null is not assignable to number
     * const value: number = option.value;
     * if(option.hasValue()){
     *      const value = option.value //This will work because the Option is identified as Some.
     * }
     * ```
     */
    abstract hasValue(): this is Some<T>;

    /**
     * Returns the value of the option as nullable.
     */
    abstract get value(): T | null;

    /**
     * This method modifies the value which is wrapped by the Option instance.
     * @param f This function is used to transform the current value to a new one.
     * @typeparam R The type of the value which is returned by f.
     * @example
     * ```typescript
     * 
     * const usernameOption: Option<string> = Option.create(findUserById(1)).map(user => user.name);
     * ```
     * @returns A new Option which wraps the modified value.
     */
    abstract map<R>(f: (v: T) => (R | null)): Option<R>;

    /**
     * This funif(Some.all<string>(foundSubstances).hasValue) {
            foundSubstance
        }ction can be used to access the value of the Option with a fallback value
     * @param defaultValue The value which will be returned if the instance behind the Option is an instance of None.
     * @returns If the instance behind the Option is an instance of Some the wrapped value will be returned, otherwise the method returns the defaultValue.
     * @example
     * ```typescript
     * 
     * const username: Option<string> = Option.create(getUserNameById(1)).getOrElse("Unknwon");
     * ```
     * */
    abstract getOrElse(defaultValue: T): T;

    /**
     * This function can be used to access the value of the Option with a fallback value.
     * @param defaultValueFactory A method which provides an value, which will be returned if the instance behind the Option is an instance of None.
     * @returns If the instance behind the Option is an instance of Some the wrapped value will be returned, otherwise the method returns the value provided by the defaultValueFactory.
     */
    abstract getOrElse(defaultValueFactory: () => T): T;

    /**
     * Creates a new Option instance.
     * If value is not null this method returns a new Some instance.
     * If value is null this method returns a new None instance.
     * @param value The value wrapped by the option.
     * @example
     * ```typescript
     * 
     * //findUser will return a User or null
     * const user = Option.create(findUserById(1)); 
     * ```
     */
    static create<T>(value: T | null): Option<T> {
        if (value != null) {
            return new Some(value);
        }
        return new None();
    }
    
    /**
     * Returns a copy of the given value, where all properties are converted to Options.
     * @example
     * ```typescript
     * 
     * const obj = {
     *  a: 1,
     *  b: null
     * }
     * const optionized = Option.optionize(obj);
     * console.log(obj.a.getOrElse(-1)); //1
     * console.log(obj.b.getOrElse(-1)) //-1
     * ```
     */
    static optionize<T extends any[]>(value: T): Option<T>[];
    static optionize<T extends Function|Function|boolean|number|string>(value: T): T;
    static optionize<T extends Object>(value: T, keys: Array<keyof T>): Optionized<T>;
    static optionize<T extends Object>(value: T, keys?: Array<keyof T>): Optionized<T>|T {
        if(Array.isArray(value)){
            return value.map(el => Option.create(el as T)) as any;
        }
        else if(typeof value == "object"){
            const copy = Object.create(value);
            for(let key of keys!){
                if(!(copy[key] instanceof Function)){
                    copy[key] = Option.create((value as any)[key]);
                }
                else{
                    copy[key] = (value as any)[key];
                }
            }
            return copy;
        }
        return value as Optionized<T>;
    }

    static all<T1>(options: [Option<T1>]): Option<[T1]>;
    static all<T1, T2>(options: [Option<T1>, Option<T2>]): Option<[T1, T2]>;
    static all<T1, T2, T3>(options: [Option<T1>, Option<T2>, Option<T3>]): Option<[T1, T2, T3]>;
    static all<T1, T2, T3, T4>(options: [Option<T1>, Option<T2>, Option<T3>, Option<T4>]): Option<[T1, T2, T3, T4]>;
    static all<T1, T2, T3, T4, T5>(options: [Option<T1>, Option<T2>, Option<T3>, Option<T4>, Option<T5>]): Option<[T1, T2, T3, T4, T5]>;
    static all(options: Option<any>[]): Option<any> {
        if(options.some(o => !o.hasValue())){
            return new None();
        }
        return new Some(options.map(o => o.value));
    }

}


/**
 * Represents a existend value.
 * @typeparam T The type of the value.
 */
export class Some<T> extends Option<T>{


    /**
     * Have a look at Option.value.
     */
    readonly value: T;

    /**
     * Retruns true.
     */
    hasValue(): this is Some<T> {
        return true;
    }


    /**
     * Creates a new instance.
     * @param value The value.
     * @example
     * 
     * ```
     * const num: Option<number> = new Some(10)
     * ```
     */
    constructor(value: T) {
        super();
        this.value = value;
    }


    /**
     * Have a look at Option.map.
     */
    map<R>(f: (v: T) => R | null): Option<R> {
        const value = f(this.value);
        if(value == null) {
            return new None<R>();
        }
        return new Some(value);
    }

    /**
     Have a look at Option.getOrElse
    */
    getOrElse(defaultValue: T): T;

    /**
     Have a look at Option.getOrElse
    */
    getOrElse(defaultValueFactory: () => T): T;


    getOrElse(defaultValue: T | (() => T)): T {
        return this.value;
    }

    /**
     * Returns the first of the provided Options which has a value.
     * If none of the Options has a value None will be returned.
     * @param options The list of Options.
     * @example
     * ```typescript
     * 
     * const stringOption = Some.first(new None(), new Some("a"), new Some("b"))
     * console.log(stringOption.value) //a
     * ```
     */
    static first<T>(... options: Option<T>[]): Option<T> {
        const some = options.find(o => o.hasValue());
        return some || new None();
    }

}

/**
 * Represent an non existend value.
 */
export class None<T> extends Option<T>{

    /**
     * Have a look at Option.value.
     */
    value: null = null;

    /**
     * Returns false.
     */
    hasValue(): this is Some<T>{
        return false;
    }

    /**
     * Creates a new instance.
     */
    constructor() {
        super();
    }

    /**
     * Have a look at Option.map.
     */
    map<R>(f: (v: T) => R | null): Option<R> {
        return new None();
    }

    /**
     Have a look at Option.getOrElse
    */
    getOrElse(defaultValue: T): T;

    /**
     Have a look at Option.getOrElse
    */
    getOrElse(defaultValueFactory: () => T): T;

    getOrElse(defaultValue: T|(() => T)): T {
        if(defaultValue instanceof Function){
            return defaultValue();
        }
        return defaultValue;
    }

}