/**
 * @module cmn-tls
 */
import {Try, TryResult} from "./Try";
import {NullReferenceError} from "../errors/NullReferenceError";
/**
 * This function tries to get a value of an nullable field.
 * @typeparam T The type of the value which could be contained in optional.
 * @param optional The field which could be null
 * @param errorMessage The message of the error which is thrown if optional contains null or undefined.
 * @returns If optional is neither undefined nor null an instance of TryResult is returned which contains the value of optional.
 * If optional is undefined or null an instance if TryResult is returned which contains an error with errorMessage as message.
 * @example
 * ```typescript
 * 
 * const userTry = tryGetValue(users.filter(u => u.mail == "user@website.com")[0], "No User found")
 * const user = userTry.catch(ex => createNewUser())
 * ```
 */
export function tryGetValue<T>(optional: T|undefined|null, errorMessage?: string): TryResult<T> {
    return Try<T>(() => {
        if(optional !== null && optional !== undefined){
            return optional as T;
        }
        throw new NullReferenceError(errorMessage);
    });
}