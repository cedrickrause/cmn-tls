/**
 * @module cmn-tls
 */

/**
 * This class represents a value of on of the two provided types (TLeft, TRight).
 * Every Either is either an intance of Left or an instance of Right.
 */
export abstract class Either<TLeft, TRight> {

    /**
     * Returns the wrapped value as native union type
     * @example
     * ```typescript
     * 
     * const union: string | number = new Left<string,number>("hallo");
     * ```
     */
    abstract readonly value: TLeft | TRight;
    
    /**
     * Applies one of the given functions on the value.
     * If the Either is a Left the function left is applied.
     * If the Either is a Right the function right is applied
     * The function returns the result of the function application.
     * @param left 
     * @param right 
     * @example
     * ```typescript
     * 
     * const str = new Left<string, number>("hallo", 10)
     *  .map(str => str, num => num.toString())
     * ```
     */
    abstract map<TResult>(left: (value: TLeft) => TResult, right: (value: TRight) => TResult): TResult;
    
    /**
     * Returns true if the Either is a Left and false if it's a Right
     */
    abstract isLeft(): this is Left<TLeft, TRight>;
    
    /**
     * Returns true if the Either is a Right and false if it's a Left
     */
    abstract isRight(): this is Right<TLeft, TRight>;

}

/**
 * This class is used to represent an value of the first generic type of Either (TLeft).
 * @example
 * ```typescript
 * 
 * const either: Either<string, number> = new Left("hello");
 * ```
 */
export class Left<TLeft, TRight> extends Either<TLeft, TRight> {

    value: TLeft;

    constructor(value: TLeft){
        super();
        this.value = value;
    }

    map<TResult>(left: (value: TLeft) => TResult, right: (value: TRight) => TResult): TResult {
        return left(this.value);
    }

    isLeft(): this is Left<TLeft, TRight> {
        return true;
    }
    isRight(): this is Right<TLeft, TRight> {
        return false;
    }

}

/**
 * This class is used to represent an value of the second generic type of Either (TRight).
 * @example
 * ```typescript
 * 
 * const either: Either<string, number> = new Right(100);
 * ```
 */
export class Right<TLeft, TRight> extends Either<TLeft, TRight> {
    value: TRight;    
    
    constructor(value: TRight){
        super();
        this.value = value;
    }

    map<TResult>(left: (value: TLeft) => TResult, right: (value: TRight) => TResult): TResult {
        return right(this.value);
    }

    isLeft(): this is Left<TLeft, TRight> {
        return false;
    }
    isRight(): this is Right<TLeft, TRight> {
        return true;
    }

}