/**
 * @module cmn-tls
 * This error is thrown when a null-reference occurs.
 */
export class NullReferenceError extends Error{

    constructor(message?: string) {
        super(message);
        Object.setPrototypeOf(this, NullReferenceError.prototype)
    }

}