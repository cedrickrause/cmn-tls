/**
 * @module cmn-tls
 * A mapped type, where all property types of the original type are changed to unknown.
 * @typeparam T the original type.
 */
export type Unsave<T> = {
    [P in keyof T]?: unknown
}