/**
 * @module cmn-tls
 */
/**
 * @ignore
 */
type NonFunctionsPropertyNames<T> = {[K in keyof T]: T[K] extends Function ? never : K}[keyof T]
/**
 * A mapped type which contains all properties of the original type, where the type of the property does not extends Function.
 * @typeparam T The original type
 * @example
 * ```typescript
 * 
 * interface Animal{
 *      name: string;
 *      makeNoise: () => void;     
 * }
 * 
 * const uselessDog: NonFunctionsOnly<Animal> = {
 *      name: "Dog"
 * }
 * ```
 */
export type NonFunctionsOnly<T> = Pick<T, NonFunctionsPropertyNames<T>>

