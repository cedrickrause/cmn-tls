/**
 * @module cmn-tls
 */

 /**
  * Replaces the type of the property K with N
  * @example
  * 
  * ```typescript
  * 
  * interface AIsString {
  *     a: string;
  *     b: number;
  * }
  * 
  * type AIsNumber = Retype<AIsString, "a", number>;
  */
export type Retype<T, K extends keyof T, N > = {[P in keyof T]: P extends K ? N : T[P]}