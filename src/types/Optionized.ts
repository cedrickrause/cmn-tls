/**
 * @module cmn-tls
 */
import { Option } from "../cmn-tls";

/**
 * Converts all propterties of T to Option<T[K]>
 */
export type Optionized<T> =  {[K in keyof T]-?: T[K] extends Function ? T[K] : Option<NonNullable<T[K]>>};
   