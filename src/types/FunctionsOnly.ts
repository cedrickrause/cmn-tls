/**
 * @module cmn-tls
 */
/**
 * @ignore
 */
type FunctionPropertyNames<T> = {[K in keyof T]: T[K] extends Function ? K : never}[keyof T];
/**
 * A mapped type which contains all properties of the original type, where the type of the property extends Function.
 * @typeparam T The original type
 * @example
 * ```typescript
 * 
 * interface Animal{
 *      name: string;
 *      makeNoise: () => void;     
 * }
 * 
 * const anonymousDog: FunctionsOnly<Animal> = {
 *      makeNoise: () => console.log("bark");
 * }
 * ```
 */
export type FunctionsOnly<T> = Pick<T, FunctionPropertyNames<T>>