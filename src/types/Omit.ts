/**
 * @module cmn-tls
 * A mapped type where the new type contains all properties of the original except those which are specified within K
 * @example
 * 
 * ```typescript
 * 
 * interface Person{
 *    id: number;
 *    name: string;
 * }
 * const userWithoutName: Omit<Person, "name"> = {
 *   id: 1
 * } 
 * ```
*/
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
